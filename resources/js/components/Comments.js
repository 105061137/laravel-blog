import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FormGroup, Col, Panel, Radio } from 'react-bootstrap';
import axios from 'axios';
import ShowMoreText from 'react-show-more-text';
import DelComments from './DelComments';


export default class Comments extends Component {
    constructor() {
        super()
        this.onChangeContent = this.onChangeContent.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        var idx = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        this.state={
            content:"",
            postid:idx,
            name:"",
            sql12271205:[]
        }

    } 

    executeOnClick(isExpanded) {
        console.log(isExpanded);
    }
    
    componentDidMount() {
        axios.get('/api/comments').then(response=>{
            this.setState({sql12271205:response.data});
        });
    }
    onChangeContent(e) {
        this.setState({
            content: e.target.value
        });
    }
    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        
        const newPost ={
            content: this.state.content,
            postid: this.state.postid,
            name: this.state.name,
            email: "abc@email.com"
        }
        axios.post('addComment', newPost).then(response=>console.log(response)).catch(error => {
            console.log(error);
          });
        // window.location = window.location;
        axios.get('/api/comments').then(response=>{
            this.setState({sql12271205:response.data});
        });

        this.state.content="";
        this.state.name="";


    }

    render() {
        
            return (
                
                <div>
                    {
                            
                            this.state.sql12271205.map((comments)=> {
                                var idx = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
                                if (comments.postid == idx) {
                                return(
                                    
                                    <div class="card border-dark" style={{ marginBottom: '1.5rem' }}>
                                        <div class="card-header ">
                                            By {comments.name} | {comments.created_at}
                                        </div>
                                        <div class="card-body" >
                                            {/* <p class="card-text"><small class="">By {comments.name} | {comments.created_at}</small></p> */}
                                            {/* <p class="card-text"><small class="text-muted"></small></p> */}
                                            <DelComments LogIn = {this.props.LogIn} user = {this.props.user} author = {this.props.author} comid={comments.id}/>
                                            <ShowMoreText
                                                lines={3}
                                                more='Show more'
                                                less='Show less'
                                                anchorClass=''
                                                onClick={this.executeOnClick}
                                            >
                                            
                                            <p class="card-text">{comments.content}</p>
                                            </ShowMoreText>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    
                                )
                                }
                            })
                        
                        
                        }
                        <div>
                            <br/>
                        </div>
                        <h3>Leave a comment</h3>
                            <form onSubmit={this.onSubmit}>
                                <div class="form-group" >
                                    <label htmlFor="title">Name</label>
                                    <input  type="text" 
                                            className="form-control" 
                                            id="title" 
                                            value = {this.state.name} 
                                            onChange = {this.onChangeName}
                                            placeholder=""></input>
                                </div>
                                <div class="form-group">
                                    <label htmlFor="exampleFormControlTextarea1">Comment</label>
                                    <textarea   className="form-control" 
                                                id="content" 
                                                value = {this.state.content} 
                                                onChange = {this.onChangeContent}
                                                rows="3"></textarea>
                                    <br/>
                                    <button type="submit" class="btn btn-secondary">Submit</button>
                                
                                </div>
                            </form>
                </div>
    
    
    
                
            );
        
        
        // return (
            
        //     <div>

        //                 {
                            
        //                     this.state.sql12271205.map((comments)=> {
        //                         var idx = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        //                         if (comments.postid == idx) {
        //                         return(
                                    
        //                             <div class="card bg-light text-black" style={{ marginBottom: '1.5rem' }}>

        //                                 <div class="card-body" >
        //                                     <p class="card-text"><small class="">By {comments.name} | {comments.created_at}</small></p>
        //                                     {/* <p class="card-text"><small class="text-muted"></small></p> */}
                                            
        //                                     <ShowMoreText
        //                                         lines={3}
        //                                         more='Show more'
        //                                         less='Show less'
        //                                         anchorClass=''
        //                                         onClick={this.executeOnClick}
        //                                     >
        //                                     <p class="card-text">{comments.content}</p>
        //                                     </ShowMoreText>
        //                                 </div>
                                        
        //                             </div>
                                    
                                    
        //                         )
        //                         }
        //                     })
                        
                        
        //                 }
        //                 <div>
        //                     <br/>
        //                 </div>
        //                 <AddComments LogIn = {this.props.LogIn}/>
        //     </div>
        // )
        }
        

    
}

// if (document.getElementById('post')) {
//     ReactDOM.render(<Post />, document.getElementById('post'));
// }
