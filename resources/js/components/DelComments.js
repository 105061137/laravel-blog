import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FormGroup, Col, Panel, Radio } from 'react-bootstrap';
import axios from 'axios';
import ShowMoreText from 'react-show-more-text';



export default class DelComments extends Component {
    constructor() {
        super()
        this.onChangeContent = this.onChangeContent.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        var idx = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        this.state={
            content:"",
            postid:idx,
        }

    } 

    onChangeContent(e) {
        this.setState({
            content: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        
        const newPost ={
            content: this.state.content,
            postid: this.state.postid,
            name: "default",
            email: "abc@email.com"
        }
        axios.post('addComment', newPost).then(response=>console.log(response)).catch(error => {
            console.log(error);
          });
        window.location = window.location;

    }
    onDelete(com_id){
        axios.delete('/api/deleteCom/'+ com_id).then(response=>{

        })
    }
    // executeOnClick(isExpanded) {
    //     console.log(isExpanded);
    // }
    
    // componentDidMount() {
    //     axios.get('/api/comments').then(response=>{
    //         this.setState({sql12271205:response.data});
    //     });
    // }
    

    render() {
        var idx = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        
        console.log("so in? ", this.props.LogIn);
        if (this.props.user == this.props.author) {
            return (
                <div>
                    <a href={"/"+idx} onClick={this.onDelete.bind(this, this.props.comid)}> Delete</a>
                </div>

    
    
                
            );
        }
        else {
            return (
                <div></div>
            )
        }
        

    }
}

// if (document.getElementById('post')) {
//     ReactDOM.render(<Post />, document.getElementById('post'));
// }
