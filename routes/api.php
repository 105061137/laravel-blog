<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/posts','Api\PostsController@index');
Route::get('/comments','Api\CommentsController@index');
Route::get('/edit/{id}', 'Api\PostsController@edit');
Route::put('/update/{id}', 'Api\PostsController@update');
Route::delete('/delete/{id}', 'Api\PostsController@destroy');
Route::delete('/deleteCom/{id}', 'Api\CommentsController@destroy');



